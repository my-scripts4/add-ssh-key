# Script to automate creation of ssh key for user and add it to local and remote hosts authorized_keys

The script was created to automate users ssh key rotation.

This script is based on the work of [Will Morgan](https://gist.githubusercontent.com/willmorgan/030046c3a0418b18ccdc8ea90f4d8093/raw/d82ff6205d40480e6c0c415e44e89308f5221da8/update-authorized-keys.sh)

## Parameters
The script receives the following parameters:
- -u username
- -k url-to-public-key
- -d #debug flag



Note: The script requires root privileges to run.

## What it does
1. If -k parameter is absent, the script assumes the creation of new key is required. 
   It generates a new rsa key pair in the user's $HOME/.ssh/ directory. 
   For user convenience, it also creates an openssh version (.pem) using putty-tools.
   Install with: `sudo apt install putty-tools`

2. The public key is added to the local authorized_keys file of the user.

3. The public key is then copied with ssh-copy-id to the server list defined in SERVERS.

## Caveats
- The user is required to have previously setup password-less login to each server in the servers list. The script is intended for key rotation on existing users and will not solve configuring  password-less logins for new users.

## Use Examples

`sudo ./addsshkey.sh -u test-user`

`sudo ./addsshkey.sh -u test-user -k https://public-key-file-location`


## ToDo
- Provide the server list by parameter, config file or environment variable
- While the script eliminates previous keys on the local host, it doesn't on remote hosts.
